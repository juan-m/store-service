package edu.umss.storeservice.model;

import javax.persistence.*;
import java.util.Objects;

/**
 * @author Juan Montaño
 */
@Entity
public class Imagen {
    private Integer idImagen;
    private String url;
    private String codigoProducto;
    private Producto productoByCodigoProducto;

    @Id
    @Column(name = "IdImagen", nullable = false)
    public Integer getIdImagen() {
        return idImagen;
    }

    public void setIdImagen(Integer idImagen) {
        this.idImagen = idImagen;
    }

    @Basic
    @Column(name = "Url", nullable = false, length = 250)
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Basic
    @Column(name = "CodigoProducto", nullable = false, length = 50)
    public String getCodigoProducto() {
        return codigoProducto;
    }

    public void setCodigoProducto(String codigoProducto) {
        this.codigoProducto = codigoProducto;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Imagen imagen = (Imagen) o;
        return Objects.equals(idImagen, imagen.idImagen) &&
                Objects.equals(url, imagen.url) &&
                Objects.equals(codigoProducto, imagen.codigoProducto);
    }

    @Override
    public int hashCode() {

        return Objects.hash(idImagen, url, codigoProducto);
    }

    @ManyToOne
    @JoinColumn(name = "CodigoProducto", referencedColumnName = "CodigoProducto", nullable = false)
    public Producto getProductoByCodigoProducto() {
        return productoByCodigoProducto;
    }

    public void setProductoByCodigoProducto(Producto productoByCodigoProducto) {
        this.productoByCodigoProducto = productoByCodigoProducto;
    }
}
