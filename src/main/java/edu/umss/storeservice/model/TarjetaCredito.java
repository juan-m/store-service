package edu.umss.storeservice.model;

import javax.persistence.*;
import java.sql.Date;
import java.util.Objects;

/**
 * @author Juan Montaño
 */
@Entity
public class TarjetaCredito {
    private Integer idTarjetaCredito;
    private String empresaBanco;
    private String numero;
    private Date fechaVencimiento;
    private String codigoSeguridad;
    private Boolean estaActivo;
    private Integer idPersona;
    private Persona personaByIdPersona;

    @Id
    @Column(name = "IdTarjetaCredito", nullable = false)
    public Integer getIdTarjetaCredito() {
        return idTarjetaCredito;
    }

    public void setIdTarjetaCredito(Integer idTarjetaCredito) {
        this.idTarjetaCredito = idTarjetaCredito;
    }

    @Basic
    @Column(name = "EmpresaBanco", nullable = false, length = 50)
    public String getEmpresaBanco() {
        return empresaBanco;
    }

    public void setEmpresaBanco(String empresaBanco) {
        this.empresaBanco = empresaBanco;
    }

    @Basic
    @Column(name = "Numero", nullable = false, length = 50)
    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    @Basic
    @Column(name = "fechaVencimiento", nullable = false)
    public Date getFechaVencimiento() {
        return fechaVencimiento;
    }

    public void setFechaVencimiento(Date fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    @Basic
    @Column(name = "CodigoSeguridad", nullable = true, length = 200)
    public String getCodigoSeguridad() {
        return codigoSeguridad;
    }

    public void setCodigoSeguridad(String codigoSeguridad) {
        this.codigoSeguridad = codigoSeguridad;
    }

    @Basic
    @Column(name = "estaActivo", nullable = false)
    public Boolean getEstaActivo() {
        return estaActivo;
    }

    public void setEstaActivo(Boolean estaActivo) {
        this.estaActivo = estaActivo;
    }

    @Basic
    @Column(name = "IdPersona", nullable = false)
    public Integer getIdPersona() {
        return idPersona;
    }

    public void setIdPersona(Integer idPersona) {
        this.idPersona = idPersona;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TarjetaCredito that = (TarjetaCredito) o;
        return Objects.equals(idTarjetaCredito, that.idTarjetaCredito) &&
                Objects.equals(empresaBanco, that.empresaBanco) &&
                Objects.equals(numero, that.numero) &&
                Objects.equals(fechaVencimiento, that.fechaVencimiento) &&
                Objects.equals(codigoSeguridad, that.codigoSeguridad) &&
                Objects.equals(estaActivo, that.estaActivo) &&
                Objects.equals(idPersona, that.idPersona);
    }

    @Override
    public int hashCode() {

        return Objects.hash(idTarjetaCredito, empresaBanco, numero, fechaVencimiento, codigoSeguridad, estaActivo, idPersona);
    }

    @ManyToOne
    @JoinColumn(name = "IdPersona", referencedColumnName = "IdPersona", nullable = false)
    public Persona getPersonaByIdPersona() {
        return personaByIdPersona;
    }

    public void setPersonaByIdPersona(Persona personaByIdPersona) {
        this.personaByIdPersona = personaByIdPersona;
    }
}
