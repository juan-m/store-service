package edu.umss.storeservice.model;

import javax.persistence.*;
import java.util.Collection;
import java.util.Objects;

/**
 * @author Juan Montaño
 */
@Entity
public class Ciudad {
    private Integer idCiudad;
    private String nombre;
    private Integer idPais;
    private Pais paisByIdPais;
    private Collection<Persona> personasByIdCiudad;

    @Id
    @Column(name = "IdCiudad", nullable = false)
    public Integer getIdCiudad() {
        return idCiudad;
    }

    public void setIdCiudad(Integer idCiudad) {
        this.idCiudad = idCiudad;
    }

    @Basic
    @Column(name = "Nombre", nullable = false, length = 100)
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "IdPais", nullable = false)
    public Integer getIdPais() {
        return idPais;
    }

    public void setIdPais(Integer idPais) {
        this.idPais = idPais;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Ciudad ciudad = (Ciudad) o;
        return Objects.equals(idCiudad, ciudad.idCiudad) &&
                Objects.equals(nombre, ciudad.nombre) &&
                Objects.equals(idPais, ciudad.idPais);
    }

    @Override
    public int hashCode() {

        return Objects.hash(idCiudad, nombre, idPais);
    }

    @ManyToOne
    @JoinColumn(name = "IdPais", referencedColumnName = "IdPais", nullable = false)
    public Pais getPaisByIdPais() {
        return paisByIdPais;
    }

    public void setPaisByIdPais(Pais paisByIdPais) {
        this.paisByIdPais = paisByIdPais;
    }

    @OneToMany(mappedBy = "ciudadByIdCiudad")
    public Collection<Persona> getPersonasByIdCiudad() {
        return personasByIdCiudad;
    }

    public void setPersonasByIdCiudad(Collection<Persona> personasByIdCiudad) {
        this.personasByIdCiudad = personasByIdCiudad;
    }
}
