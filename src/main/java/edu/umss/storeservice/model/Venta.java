package edu.umss.storeservice.model;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.Objects;

/**
 * @author Juan Montaño
 */
@Entity
public class Venta {
    private Integer idVenta;
    private Integer nroTransaccion;
    private Timestamp fecha;
    private String concepto;
    private Integer idPersona;
    private Collection<Detalle> detallesByIdVenta;
    private Persona personaByIdPersona;

    @Id
    @Column(name = "IdVenta", nullable = false)
    public Integer getIdVenta() {
        return idVenta;
    }

    public void setIdVenta(Integer idVenta) {
        this.idVenta = idVenta;
    }

    @Basic
    @Column(name = "NroTransaccion", nullable = false)
    public Integer getNroTransaccion() {
        return nroTransaccion;
    }

    public void setNroTransaccion(Integer nroTransaccion) {
        this.nroTransaccion = nroTransaccion;
    }

    @Basic
    @Column(name = "Fecha", nullable = false)
    public Timestamp getFecha() {
        return fecha;
    }

    public void setFecha(Timestamp fecha) {
        this.fecha = fecha;
    }

    @Basic
    @Column(name = "Concepto", nullable = true, length = 250)
    public String getConcepto() {
        return concepto;
    }

    public void setConcepto(String concepto) {
        this.concepto = concepto;
    }

    @Basic
    @Column(name = "IdPersona", nullable = false)
    public Integer getIdPersona() {
        return idPersona;
    }

    public void setIdPersona(Integer idPersona) {
        this.idPersona = idPersona;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Venta venta = (Venta) o;
        return Objects.equals(idVenta, venta.idVenta) &&
                Objects.equals(nroTransaccion, venta.nroTransaccion) &&
                Objects.equals(fecha, venta.fecha) &&
                Objects.equals(concepto, venta.concepto) &&
                Objects.equals(idPersona, venta.idPersona);
    }

    @Override
    public int hashCode() {

        return Objects.hash(idVenta, nroTransaccion, fecha, concepto, idPersona);
    }

    @OneToMany(mappedBy = "ventaByIdVenta")
    public Collection<Detalle> getDetallesByIdVenta() {
        return detallesByIdVenta;
    }

    public void setDetallesByIdVenta(Collection<Detalle> detallesByIdVenta) {
        this.detallesByIdVenta = detallesByIdVenta;
    }

    @ManyToOne
    @JoinColumn(name = "IdPersona", referencedColumnName = "IdPersona", nullable = false)
    public Persona getPersonaByIdPersona() {
        return personaByIdPersona;
    }

    public void setPersonaByIdPersona(Persona personaByIdPersona) {
        this.personaByIdPersona = personaByIdPersona;
    }
}
