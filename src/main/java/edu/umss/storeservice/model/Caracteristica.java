package edu.umss.storeservice.model;

import javax.persistence.*;
import java.util.Objects;

/**
 * @author Juan Montaño
 */
@Entity
public class Caracteristica {
    private Integer idCaracteristica;
    private String campo;
    private String valor;
    private String codigoProducto;
    private Producto productoByCodigoProducto;

    @Id
    @Column(name = "IdCaracteristica", nullable = false)
    public Integer getIdCaracteristica() {
        return idCaracteristica;
    }

    public void setIdCaracteristica(Integer idCaracteristica) {
        this.idCaracteristica = idCaracteristica;
    }

    @Basic
    @Column(name = "Campo", nullable = false, length = 150)
    public String getCampo() {
        return campo;
    }

    public void setCampo(String campo) {
        this.campo = campo;
    }

    @Basic
    @Column(name = "Valor", nullable = false, length = 150)
    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    @Basic
    @Column(name = "CodigoProducto", nullable = false, length = 50)
    public String getCodigoProducto() {
        return codigoProducto;
    }

    public void setCodigoProducto(String codigoProducto) {
        this.codigoProducto = codigoProducto;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Caracteristica that = (Caracteristica) o;
        return Objects.equals(idCaracteristica, that.idCaracteristica) &&
                Objects.equals(campo, that.campo) &&
                Objects.equals(valor, that.valor) &&
                Objects.equals(codigoProducto, that.codigoProducto);
    }

    @Override
    public int hashCode() {

        return Objects.hash(idCaracteristica, campo, valor, codigoProducto);
    }

    @ManyToOne
    @JoinColumn(name = "CodigoProducto", referencedColumnName = "CodigoProducto", nullable = false)
    public Producto getProductoByCodigoProducto() {
        return productoByCodigoProducto;
    }

    public void setProductoByCodigoProducto(Producto productoByCodigoProducto) {
        this.productoByCodigoProducto = productoByCodigoProducto;
    }
}
