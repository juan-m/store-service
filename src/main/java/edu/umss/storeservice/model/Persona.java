package edu.umss.storeservice.model;

import javax.persistence.*;
import java.sql.Date;
import java.util.Collection;
import java.util.Objects;

/**
 * @author Juan Montaño
 */
@Entity
public class Persona {
    private Integer idPersona;
    private String ci;
    private String nombre;
    private String apellido;
    private String celular;
    private Date fechaNacimiento;
    private String direccion;
    private Integer idCiudad;
    private Integer idUsuario;
    private Integer idEmpresa;
    private Ciudad ciudadByIdCiudad;
    private Usuario usuarioByIdUsuario;
    private Empresa empresaByIdEmpresa;
    private Collection<Producto> productosByIdPersona;
    private Collection<TarjetaCredito> tarjetaCreditosByIdPersona;
    private Collection<Venta> ventasByIdPersona;

    @Id
    @Column(name = "IdPersona", nullable = false)
    public Integer getIdPersona() {
        return idPersona;
    }

    public void setIdPersona(Integer idPersona) {
        this.idPersona = idPersona;
    }

    @Basic
    @Column(name = "Ci", nullable = false, length = 15)
    public String getCi() {
        return ci;
    }

    public void setCi(String ci) {
        this.ci = ci;
    }

    @Basic
    @Column(name = "Nombre", nullable = false, length = 50)
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "Apellido", nullable = true, length = 50)
    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    @Basic
    @Column(name = "Celular", nullable = true, length = 20)
    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    @Basic
    @Column(name = "FechaNacimiento", nullable = false)
    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    @Basic
    @Column(name = "Direccion", nullable = true, length = 250)
    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    @Basic
    @Column(name = "IdCiudad", nullable = true)
    public Integer getIdCiudad() {
        return idCiudad;
    }

    public void setIdCiudad(Integer idCiudad) {
        this.idCiudad = idCiudad;
    }

    @Basic
    @Column(name = "IdUsuario", nullable = false)
    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    @Basic
    @Column(name = "IdEmpresa", nullable = true)
    public Integer getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Persona persona = (Persona) o;
        return Objects.equals(idPersona, persona.idPersona) &&
                Objects.equals(ci, persona.ci) &&
                Objects.equals(nombre, persona.nombre) &&
                Objects.equals(apellido, persona.apellido) &&
                Objects.equals(celular, persona.celular) &&
                Objects.equals(fechaNacimiento, persona.fechaNacimiento) &&
                Objects.equals(direccion, persona.direccion) &&
                Objects.equals(idCiudad, persona.idCiudad) &&
                Objects.equals(idUsuario, persona.idUsuario) &&
                Objects.equals(idEmpresa, persona.idEmpresa);
    }

    @Override
    public int hashCode() {

        return Objects.hash(idPersona, ci, nombre, apellido, celular, fechaNacimiento, direccion, idCiudad, idUsuario, idEmpresa);
    }

    @ManyToOne
    @JoinColumn(name = "IdCiudad", referencedColumnName = "IdCiudad")
    public Ciudad getCiudadByIdCiudad() {
        return ciudadByIdCiudad;
    }

    public void setCiudadByIdCiudad(Ciudad ciudadByIdCiudad) {
        this.ciudadByIdCiudad = ciudadByIdCiudad;
    }

    @ManyToOne
    @JoinColumn(name = "IdUsuario", referencedColumnName = "IdUsuario", nullable = false)
    public Usuario getUsuarioByIdUsuario() {
        return usuarioByIdUsuario;
    }

    public void setUsuarioByIdUsuario(Usuario usuarioByIdUsuario) {
        this.usuarioByIdUsuario = usuarioByIdUsuario;
    }

    @ManyToOne
    @JoinColumn(name = "IdEmpresa", referencedColumnName = "IdEmpresa")
    public Empresa getEmpresaByIdEmpresa() {
        return empresaByIdEmpresa;
    }

    public void setEmpresaByIdEmpresa(Empresa empresaByIdEmpresa) {
        this.empresaByIdEmpresa = empresaByIdEmpresa;
    }

    @OneToMany(mappedBy = "personaByIdPersona")
    public Collection<Producto> getProductosByIdPersona() {
        return productosByIdPersona;
    }

    public void setProductosByIdPersona(Collection<Producto> productosByIdPersona) {
        this.productosByIdPersona = productosByIdPersona;
    }

    @OneToMany(mappedBy = "personaByIdPersona")
    public Collection<TarjetaCredito> getTarjetaCreditosByIdPersona() {
        return tarjetaCreditosByIdPersona;
    }

    public void setTarjetaCreditosByIdPersona(Collection<TarjetaCredito> tarjetaCreditosByIdPersona) {
        this.tarjetaCreditosByIdPersona = tarjetaCreditosByIdPersona;
    }

    @OneToMany(mappedBy = "personaByIdPersona")
    public Collection<Venta> getVentasByIdPersona() {
        return ventasByIdPersona;
    }

    public void setVentasByIdPersona(Collection<Venta> ventasByIdPersona) {
        this.ventasByIdPersona = ventasByIdPersona;
    }
}
