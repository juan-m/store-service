package edu.umss.storeservice.model;

import javax.persistence.*;
import java.util.Collection;
import java.util.Objects;

/**
 * @author Juan Montaño
 */
@Entity
public class Categoria {
    private Integer idCategoria;
    private String descripcion;
    private Collection<Producto> productosByIdCategoria;

    @Id
    @Column(name = "IdCategoria", nullable = false)
    public Integer getIdCategoria() {
        return idCategoria;
    }

    public void setIdCategoria(Integer idCategoria) {
        this.idCategoria = idCategoria;
    }

    @Basic
    @Column(name = "Descripcion", nullable = false, length = 50)
    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Categoria categoria = (Categoria) o;
        return Objects.equals(idCategoria, categoria.idCategoria) &&
                Objects.equals(descripcion, categoria.descripcion);
    }

    @Override
    public int hashCode() {

        return Objects.hash(idCategoria, descripcion);
    }

    @OneToMany(mappedBy = "categoriaByIdCategoria")
    public Collection<Producto> getProductosByIdCategoria() {
        return productosByIdCategoria;
    }

    public void setProductosByIdCategoria(Collection<Producto> productosByIdCategoria) {
        this.productosByIdCategoria = productosByIdCategoria;
    }
}
