package edu.umss.storeservice.model;

import javax.persistence.*;
import java.util.Collection;
import java.util.Objects;

/**
 * @author Juan Montaño
 */
@Entity
public class Pais {
    private Integer idPais;
    private String nombre;
    private Collection<Ciudad> ciudadsByIdPais;

    @Id
    @Column(name = "IdPais", nullable = false)
    public Integer getIdPais() {
        return idPais;
    }

    public void setIdPais(Integer idPais) {
        this.idPais = idPais;
    }

    @Basic
    @Column(name = "Nombre", nullable = false, length = 100)
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pais pais = (Pais) o;
        return Objects.equals(idPais, pais.idPais) &&
                Objects.equals(nombre, pais.nombre);
    }

    @Override
    public int hashCode() {

        return Objects.hash(idPais, nombre);
    }

    @OneToMany(mappedBy = "paisByIdPais")
    public Collection<Ciudad> getCiudadsByIdPais() {
        return ciudadsByIdPais;
    }

    public void setCiudadsByIdPais(Collection<Ciudad> ciudadsByIdPais) {
        this.ciudadsByIdPais = ciudadsByIdPais;
    }
}
