package edu.umss.storeservice.model;

import javax.persistence.*;
import java.sql.Date;
import java.util.Collection;
import java.util.Objects;

/**
 * @author Juan Montaño
 */
@Entity
public class Empresa {
    private Integer idEmpresa;
    private String nombre;
    private String telefono;
    private String direccion;
    private Date fundacion;
    private Collection<Persona> personasByIdEmpresa;

    @Id
    @Column(name = "IdEmpresa", nullable = false)
    public Integer getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    @Basic
    @Column(name = "Nombre", nullable = false, length = 100)
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "Telefono", nullable = true, length = 20)
    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    @Basic
    @Column(name = "Direccion", nullable = true, length = 250)
    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    @Basic
    @Column(name = "Fundacion", nullable = false)
    public Date getFundacion() {
        return fundacion;
    }

    public void setFundacion(Date fundacion) {
        this.fundacion = fundacion;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Empresa empresa = (Empresa) o;
        return Objects.equals(idEmpresa, empresa.idEmpresa) &&
                Objects.equals(nombre, empresa.nombre) &&
                Objects.equals(telefono, empresa.telefono) &&
                Objects.equals(direccion, empresa.direccion) &&
                Objects.equals(fundacion, empresa.fundacion);
    }

    @Override
    public int hashCode() {

        return Objects.hash(idEmpresa, nombre, telefono, direccion, fundacion);
    }

    @OneToMany(mappedBy = "empresaByIdEmpresa")
    public Collection<Persona> getPersonasByIdEmpresa() {
        return personasByIdEmpresa;
    }

    public void setPersonasByIdEmpresa(Collection<Persona> personasByIdEmpresa) {
        this.personasByIdEmpresa = personasByIdEmpresa;
    }
}
