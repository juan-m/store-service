package edu.umss.storeservice.model;

import javax.persistence.*;
import java.util.Collection;
import java.util.Objects;

/**
 * @author Juan Montaño
 */
@Entity
public class Rol {
    private Integer idRol;
    private String nombreRol;
    private Collection<Usuario> usuariosByIdRol;

    @Id
    @Column(name = "IdRol", nullable = false)
    public Integer getIdRol() {
        return idRol;
    }

    public void setIdRol(Integer idRol) {
        this.idRol = idRol;
    }

    @Basic
    @Column(name = "Nombre_rol", nullable = false, length = 100)
    public String getNombreRol() {
        return nombreRol;
    }

    public void setNombreRol(String nombreRol) {
        this.nombreRol = nombreRol;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Rol rol = (Rol) o;
        return Objects.equals(idRol, rol.idRol) &&
                Objects.equals(nombreRol, rol.nombreRol);
    }

    @Override
    public int hashCode() {

        return Objects.hash(idRol, nombreRol);
    }

    @OneToMany(mappedBy = "rolByIdRol")
    public Collection<Usuario> getUsuariosByIdRol() {
        return usuariosByIdRol;
    }

    public void setUsuariosByIdRol(Collection<Usuario> usuariosByIdRol) {
        this.usuariosByIdRol = usuariosByIdRol;
    }
}
