package edu.umss.storeservice.model;

import javax.persistence.*;
import java.util.Objects;

/**
 * @author Juan Montaño
 */
@Entity
public class Detalle {
    private Integer idDetalle;
    private Integer cantidad;
    private Long precio;
    private Long subTotal;
    private String codigoProducto;
    private Integer idVenta;
    private Producto productoByCodigoProducto;
    private Venta ventaByIdVenta;

    @Id
    @Column(name = "IdDetalle", nullable = false)
    public Integer getIdDetalle() {
        return idDetalle;
    }

    public void setIdDetalle(Integer idDetalle) {
        this.idDetalle = idDetalle;
    }

    @Basic
    @Column(name = "Cantidad", nullable = false)
    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    @Basic
    @Column(name = "Precio", nullable = false)
    public Long getPrecio() {
        return precio;
    }

    public void setPrecio(Long precio) {
        this.precio = precio;
    }

    @Basic
    @Column(name = "SubTotal", nullable = true)
    public Long getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(Long subTotal) {
        this.subTotal = subTotal;
    }

    @Basic
    @Column(name = "CodigoProducto", nullable = false, length = 50)
    public String getCodigoProducto() {
        return codigoProducto;
    }

    public void setCodigoProducto(String codigoProducto) {
        this.codigoProducto = codigoProducto;
    }

    @Basic
    @Column(name = "IdVenta", nullable = false)
    public Integer getIdVenta() {
        return idVenta;
    }

    public void setIdVenta(Integer idVenta) {
        this.idVenta = idVenta;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Detalle detalle = (Detalle) o;
        return Objects.equals(idDetalle, detalle.idDetalle) &&
                Objects.equals(cantidad, detalle.cantidad) &&
                Objects.equals(precio, detalle.precio) &&
                Objects.equals(subTotal, detalle.subTotal) &&
                Objects.equals(codigoProducto, detalle.codigoProducto) &&
                Objects.equals(idVenta, detalle.idVenta);
    }

    @Override
    public int hashCode() {

        return Objects.hash(idDetalle, cantidad, precio, subTotal, codigoProducto, idVenta);
    }

    @ManyToOne
    @JoinColumn(name = "CodigoProducto", referencedColumnName = "CodigoProducto", nullable = false)
    public Producto getProductoByCodigoProducto() {
        return productoByCodigoProducto;
    }

    public void setProductoByCodigoProducto(Producto productoByCodigoProducto) {
        this.productoByCodigoProducto = productoByCodigoProducto;
    }

    @ManyToOne
    @JoinColumn(name = "IdVenta", referencedColumnName = "IdVenta", nullable = false)
    public Venta getVentaByIdVenta() {
        return ventaByIdVenta;
    }

    public void setVentaByIdVenta(Venta ventaByIdVenta) {
        this.ventaByIdVenta = ventaByIdVenta;
    }
}
