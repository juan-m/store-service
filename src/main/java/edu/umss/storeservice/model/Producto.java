package edu.umss.storeservice.model;

import javax.persistence.*;
import java.util.Collection;
import java.util.Objects;

/**
 * @author Juan Montaño
 */
@Entity
public class Producto {
    private String codigoProducto;
    private String nombre;
    private String marca;
    private String descripcion;
    private Long precio;
    private Integer porcentajeOferta;
    private Integer idCategoria;
    private Integer idPersona;
    private Collection<Caracteristica> caracteristicasByCodigoProducto;
    private Collection<Detalle> detallesByCodigoProducto;
    private Collection<Imagen> imagensByCodigoProducto;
    private Categoria categoriaByIdCategoria;
    private Persona personaByIdPersona;
    private Collection<RegistroIngreso> registroIngresosByCodigoProducto;

    @Id
    @Column(name = "CodigoProducto", nullable = false, length = 50)
    public String getCodigoProducto() {
        return codigoProducto;
    }

    public void setCodigoProducto(String codigoProducto) {
        this.codigoProducto = codigoProducto;
    }

    @Basic
    @Column(name = "Nombre", nullable = false, length = 150)
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "Marca", nullable = false, length = 100)
    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    @Basic
    @Column(name = "Descripcion", nullable = true, length = 2147483647)
    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Basic
    @Column(name = "Precio", nullable = false)
    public Long getPrecio() {
        return precio;
    }

    public void setPrecio(Long precio) {
        this.precio = precio;
    }

    @Basic
    @Column(name = "PorcentajeOferta", nullable = true)
    public Integer getPorcentajeOferta() {
        return porcentajeOferta;
    }

    public void setPorcentajeOferta(Integer porcentajeOferta) {
        this.porcentajeOferta = porcentajeOferta;
    }

    @Basic
    @Column(name = "IdCategoria", nullable = false)
    public Integer getIdCategoria() {
        return idCategoria;
    }

    public void setIdCategoria(Integer idCategoria) {
        this.idCategoria = idCategoria;
    }

    @Basic
    @Column(name = "IdPersona", nullable = true)
    public Integer getIdPersona() {
        return idPersona;
    }

    public void setIdPersona(Integer idPersona) {
        this.idPersona = idPersona;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Producto producto = (Producto) o;
        return Objects.equals(codigoProducto, producto.codigoProducto) &&
                Objects.equals(nombre, producto.nombre) &&
                Objects.equals(marca, producto.marca) &&
                Objects.equals(descripcion, producto.descripcion) &&
                Objects.equals(precio, producto.precio) &&
                Objects.equals(porcentajeOferta, producto.porcentajeOferta) &&
                Objects.equals(idCategoria, producto.idCategoria) &&
                Objects.equals(idPersona, producto.idPersona);
    }

    @Override
    public int hashCode() {

        return Objects.hash(codigoProducto, nombre, marca, descripcion, precio, porcentajeOferta, idCategoria, idPersona);
    }

    @OneToMany(mappedBy = "productoByCodigoProducto")
    public Collection<Caracteristica> getCaracteristicasByCodigoProducto() {
        return caracteristicasByCodigoProducto;
    }

    public void setCaracteristicasByCodigoProducto(Collection<Caracteristica> caracteristicasByCodigoProducto) {
        this.caracteristicasByCodigoProducto = caracteristicasByCodigoProducto;
    }

    @OneToMany(mappedBy = "productoByCodigoProducto")
    public Collection<Detalle> getDetallesByCodigoProducto() {
        return detallesByCodigoProducto;
    }

    public void setDetallesByCodigoProducto(Collection<Detalle> detallesByCodigoProducto) {
        this.detallesByCodigoProducto = detallesByCodigoProducto;
    }

    @OneToMany(mappedBy = "productoByCodigoProducto")
    public Collection<Imagen> getImagensByCodigoProducto() {
        return imagensByCodigoProducto;
    }

    public void setImagensByCodigoProducto(Collection<Imagen> imagensByCodigoProducto) {
        this.imagensByCodigoProducto = imagensByCodigoProducto;
    }

    @ManyToOne
    @JoinColumn(name = "IdCategoria", referencedColumnName = "IdCategoria", nullable = false)
    public Categoria getCategoriaByIdCategoria() {
        return categoriaByIdCategoria;
    }

    public void setCategoriaByIdCategoria(Categoria categoriaByIdCategoria) {
        this.categoriaByIdCategoria = categoriaByIdCategoria;
    }

    @ManyToOne
    @JoinColumn(name = "IdPersona", referencedColumnName = "IdPersona")
    public Persona getPersonaByIdPersona() {
        return personaByIdPersona;
    }

    public void setPersonaByIdPersona(Persona personaByIdPersona) {
        this.personaByIdPersona = personaByIdPersona;
    }

    @OneToMany(mappedBy = "productoByCodigoProducto")
    public Collection<RegistroIngreso> getRegistroIngresosByCodigoProducto() {
        return registroIngresosByCodigoProducto;
    }

    public void setRegistroIngresosByCodigoProducto(Collection<RegistroIngreso> registroIngresosByCodigoProducto) {
        this.registroIngresosByCodigoProducto = registroIngresosByCodigoProducto;
    }
}
