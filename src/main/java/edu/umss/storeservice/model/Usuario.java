package edu.umss.storeservice.model;

import javax.persistence.*;
import java.util.Arrays;
import java.util.Collection;
import java.util.Objects;

/**
 * @author Juan Montaño
 */
@Entity
public class Usuario {
    private Integer idUsuario;
    private String email;
    private byte[] password;
    private Boolean estado;
    private Integer idRol;
    private Collection<Persona> personasByIdUsuario;
    private Rol rolByIdRol;

    @Id
    @Column(name = "IdUsuario", nullable = false)
    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    @Basic
    @Column(name = "Email", nullable = false, length = 100)
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Basic
    @Column(name = "Password", nullable = false)
    public byte[] getPassword() {
        return password;
    }

    public void setPassword(byte[] password) {
        this.password = password;
    }

    @Basic
    @Column(name = "Estado", nullable = false)
    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

    @Basic
    @Column(name = "IdRol", nullable = false)
    public Integer getIdRol() {
        return idRol;
    }

    public void setIdRol(Integer idRol) {
        this.idRol = idRol;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Usuario usuario = (Usuario) o;
        return Objects.equals(idUsuario, usuario.idUsuario) &&
                Objects.equals(email, usuario.email) &&
                Arrays.equals(password, usuario.password) &&
                Objects.equals(estado, usuario.estado) &&
                Objects.equals(idRol, usuario.idRol);
    }

    @Override
    public int hashCode() {

        int result = Objects.hash(idUsuario, email, estado, idRol);
        result = 31 * result + Arrays.hashCode(password);
        return result;
    }

    @OneToMany(mappedBy = "usuarioByIdUsuario")
    public Collection<Persona> getPersonasByIdUsuario() {
        return personasByIdUsuario;
    }

    public void setPersonasByIdUsuario(Collection<Persona> personasByIdUsuario) {
        this.personasByIdUsuario = personasByIdUsuario;
    }

    @ManyToOne
    @JoinColumn(name = "IdRol", referencedColumnName = "IdRol", nullable = false)
    public Rol getRolByIdRol() {
        return rolByIdRol;
    }

    public void setRolByIdRol(Rol rolByIdRol) {
        this.rolByIdRol = rolByIdRol;
    }
}
