package edu.umss.storeservice.model;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

/**
 * @author Juan Montaño
 */
@Entity
public class RegistroIngreso {
    private Integer idRegistroIngreso;
    private Timestamp fecha;
    private Integer cantidad;
    private String codigoProducto;
    private Integer idProveedor;
    private Producto productoByCodigoProducto;
    private Proveedor proveedorByIdProveedor;

    @Id
    @Column(name = "IdRegistroIngreso", nullable = false)
    public Integer getIdRegistroIngreso() {
        return idRegistroIngreso;
    }

    public void setIdRegistroIngreso(Integer idRegistroIngreso) {
        this.idRegistroIngreso = idRegistroIngreso;
    }

    @Basic
    @Column(name = "Fecha", nullable = false)
    public Timestamp getFecha() {
        return fecha;
    }

    public void setFecha(Timestamp fecha) {
        this.fecha = fecha;
    }

    @Basic
    @Column(name = "Cantidad", nullable = false)
    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    @Basic
    @Column(name = "CodigoProducto", nullable = false, length = 50)
    public String getCodigoProducto() {
        return codigoProducto;
    }

    public void setCodigoProducto(String codigoProducto) {
        this.codigoProducto = codigoProducto;
    }

    @Basic
    @Column(name = "IdProveedor", nullable = true)
    public Integer getIdProveedor() {
        return idProveedor;
    }

    public void setIdProveedor(Integer idProveedor) {
        this.idProveedor = idProveedor;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RegistroIngreso that = (RegistroIngreso) o;
        return Objects.equals(idRegistroIngreso, that.idRegistroIngreso) &&
                Objects.equals(fecha, that.fecha) &&
                Objects.equals(cantidad, that.cantidad) &&
                Objects.equals(codigoProducto, that.codigoProducto) &&
                Objects.equals(idProveedor, that.idProveedor);
    }

    @Override
    public int hashCode() {

        return Objects.hash(idRegistroIngreso, fecha, cantidad, codigoProducto, idProveedor);
    }

    @ManyToOne
    @JoinColumn(name = "CodigoProducto", referencedColumnName = "CodigoProducto", nullable = false)
    public Producto getProductoByCodigoProducto() {
        return productoByCodigoProducto;
    }

    public void setProductoByCodigoProducto(Producto productoByCodigoProducto) {
        this.productoByCodigoProducto = productoByCodigoProducto;
    }

    @ManyToOne
    @JoinColumn(name = "IdProveedor", referencedColumnName = "IdProveedor")
    public Proveedor getProveedorByIdProveedor() {
        return proveedorByIdProveedor;
    }

    public void setProveedorByIdProveedor(Proveedor proveedorByIdProveedor) {
        this.proveedorByIdProveedor = proveedorByIdProveedor;
    }
}
